package ru.t1.avfilippov.tm;

import ru.t1.avfilippov.tm.constant.ArgumentConst;
import ru.t1.avfilippov.tm.constant.CommandConst;

import java.util.Scanner;


public final class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    public static void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            default:
                showErrorArgument();
                break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.EXIT:
                showExit();
                break;
            default:
                showErrorCommand();
                break;
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Filippov");
        System.out.println("Email: avfilippov@t1-consulting.ru");
    }

    private static void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - show developer's info\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - show application version\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - show command list\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - close application\n", CommandConst.EXIT);
    }

}
